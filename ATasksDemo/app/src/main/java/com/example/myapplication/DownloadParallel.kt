package com.example.myapplication


import android.content.Context
import android.os.AsyncTask
import android.util.Log
import android.widget.Toast
import java.net.URL


public class DownloadFilesTask() : AsyncTask<String, Int, Long>() {

    var text: String = ""

    companion object {
        var i: Int = 0
        var startTime = System.currentTimeMillis()
    }

    override fun doInBackground(vararg urls: String): Long? {
        text = URL(urls[0]).readText()
        return 0
    }


    override fun onPostExecute(result: Long?) {
        i++
        Log.d("Down", "Downloaded async $text(...)")
        if (i == 500) {
            Log.d("DOWN", "Finished all async in ${System.currentTimeMillis() - startTime} millis")
        }


    }


}

