package com.example.myapplication


import android.content.Context
import android.os.AsyncTask
import android.util.Log
import android.widget.Toast
import java.net.URL


public class DownloadSequentially() : AsyncTask<String, Int, Long>() {

    var text: String = ""
    var startTime = System.currentTimeMillis()

    override fun doInBackground(vararg urls: String): Long? {

        for (i in 0..499) {
            text = URL(urls[0]).readText()
            Log.d("Down", "Downloaded seq. $text")
        }

        return 0
    }


    override fun onPostExecute(result: Long?) {
        Log.d("DOWN", "Finished all sequential in ${System.currentTimeMillis() - startTime} millis")
    }


}

