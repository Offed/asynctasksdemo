package com.example.myapplication

import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import java.net.URL


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        for (i in 0..499) {
            //Log.d("Down", "Starting task $i")
            DownloadFilesTask().execute("https://dog.ceo/api/breeds/image/random")
        }

        DownloadSequentially().execute("https://dog.ceo/api/breeds/image/random")

        Log.d("Down", "Main end")

        for (i in 0..4) {
            //Log.d("Down", "Starting task $i")
            SleepParallel().execute()
        }
    }
}
